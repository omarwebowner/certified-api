<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Schools extends Model
{
    use HasFactory;
    protected $fillable = [
        'user_id', 'level', 'type', 'phone', 'mobile', 'address', 'city', 'country', 'activities', 'establish_year', 'no_of_labs', 'type_of_labs', 'capacity_of_labs', 'status','cv',
   ];

   
   public function user()
   {
       return $this->belongsTo('App\Models\User', 'id', 'user_id');
   } 


    protected  $appends=['file']; //Make it available in the json response
    
    public function getFileAttribute()
    {
       if($this->attributes['cv'])
       {
           return asset('school/cv/'.$this->attributes['cv']);
       }
       else{
           return Null ;
       }

    }
}
