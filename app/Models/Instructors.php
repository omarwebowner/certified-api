<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Instructors extends Model
{
    use HasFactory;
    protected $fillable = [
        'user_id', 'level', 'type', 'phone', 'address', 'city', 'country', 'study_field', 'graduation_year', 'status','cv','national_id'
   ];
   public function user()
   {
       return $this->belongsTo('App\Models\User', 'id', 'user_id');
   } 


    protected  $appends=['file']; //Make it available in the json response
    
    public function getFileAttribute()
    {
       if($this->attributes['cv'])
       {
           return asset('instructor/cv/'.$this->attributes['cv']);
       }
       else{
           return Null ;
       }

    }
}
