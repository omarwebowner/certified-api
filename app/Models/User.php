<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Tymon\JWTAuth\Contracts\JWTSubject;
class User  extends Authenticatable implements JWTSubject
{
    use HasFactory, Notifiable;

     /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','role'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    public function requestCertified()
    {
        if($this->role == 'instructor'){
            return $this->hasOne('App\Models\Instructors', 'user_id', 'id');
        }
        if($this->role == 'school'){
            return $this->hasOne('App\Models\Schools', 'user_id', 'id');
        }
        if($this->role == 'lab'){
            return $this->hasOne('App\Models\Labs', 'user_id', 'id');
        }
        if($this->role == 'institute'){
            return $this->hasOne('App\Models\Institutes', 'user_id', 'id');
        }
    } 

    public function lab(){
        return $this->hasOne('App\Models\Labs', 'user_id', 'id');

    }

    public function school(){
        return $this->hasOne('App\Models\Schools', 'user_id', 'id');

    }

    public function institute(){
        return $this->hasOne('App\Models\Institutes', 'user_id', 'id');

    }

    public function instructor(){
        return $this->hasOne('App\Models\Instructors', 'user_id', 'id');

    }

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
}
