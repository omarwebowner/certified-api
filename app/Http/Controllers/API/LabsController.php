<?php

namespace App\Http\Controllers\API;

use App\Models\Labs;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;
use App\Models\User;
use Mail;
use App\Mail\Active;

use App\Http\Tools\RequestValidator as RV;
use  Validator;

class LabsController extends Controller
{
      //$RV => holds the validation controller

      protected $RV;

      //Constructing the auth controller and exclude create from authenticating
  
      public function __construct()
      {
          $this->middleware('auth:api', ['except' => ['store']]);
  
          //Assing the instance of Request Validator to protected variable $RV
  
          $this->RV = new RV;
      }
  
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       

         $Labs=User::with('lab')->has('lab')->paginate(15);
         
        if( $Labs )
         {
         $statusCode = 200;
         $response["data"] =    $Labs;
         $response["status"] = true;
         $response['message'] = "Request Successfully";
        
         return response()->json( $response,200);
 
         }
         else{
             $statusCode = 400;
             $response["status"] = false;
             $response['message'] = "there is no data to show ";
             return response()->json($response, $statusCode);
         }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $ErrArr = [];
        //Start validating the request params
        $validator = $this->RV->Validate_lap((array) $request->all());
        //in case of validation failure output error messages and fields 

        if ($validator->fails())
        {
            foreach ($validator->messages()->getMessages() as $field_name => $messages)
            {
                $ErrArr[$field_name] = $messages[0]; 
            }
            return  response()->json($ErrArr);
        }

        // Creating new user

        $User = new User;
        $User->name = $request->name;
        $User->email = $request->email;
        $User->role = 'lab';
        $User->password =  Hash::make($request->password);
        $User->save();

        // Creating instructor request to save it in the DB
        
        $Labs = new Labs;
        $Labs->user_id = $User->id;
        $Labs->level = $request->level;
        $Labs->type = $request->type;
        $Labs->phone = $request->phone;
        $Labs->mobile = $request->mobile;
        $Labs->address = $request->address;
        $Labs->city = $request->city;
        $Labs->country = $request->country;
        $Labs->activities = $request->activities;
        $Labs->establish_year = $request->establish_year;
        $Labs->no_of_labs = $request->no_of_labs;
        $Labs->type_of_labs = $request->type_of_labs;
        $Labs->capacity_of_labs = $request->capacity_of_labs;
       //// upload  cv  ///////////////////////
       $fileName = '';
       if($request->hasFile('cv'))
       {
            $cv = $request->file('cv');
            $name = $cv->getClientOriginalName(); // get image name
            $extension = $cv->getClientOriginalExtension(); // get image extension
            $sha1 = sha1($name); // hash the image name
            $random = rand(1, 1000000); // Random To Name
            $name_database = $random . "_" . date("y-m-d-h-i-s") . "_" . $sha1; // To use it without extension
            $fileName = $random . "_" . date("y-m-d-h-i-s") . "_" . $sha1 . "." . $extension;
            $cv->move(public_path('lab/cv'),  $fileName); //folder logo in public must you create
            $request->cv = $fileName;
            
       }
       ///////////// end upload cv  //////////////
        $Labs->cv = $fileName;

       ///////////////////// end  upload 
        $Labs->status = $request->status;
        $Labs->save();
        $mail= Mail::to($request->email)->send(new Active($Labs));
        $data['data'] = ['msg' => 'Request Success','status' => 1];
        return response()->json($data);
    }

   
    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Labs  $Labs
     * @return \Illuminate\Http\Response
     */
    public function show(Labs $Labs)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Labs  $Labs
     * @return \Illuminate\Http\Response
     */
    public function edit(Labs $Labs)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Labs  $Labs
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $this->authorize('isAdmin');
        $ErrArr = [];
                
        //Start validating the request params
        
        $validator = $this->RV->Validate_updatelap((array) $request->all(),$request->user_id);

        //in case of validation failure output error messages and fields 

        if ($validator->fails())
        {
            foreach ($validator->messages()->getMessages() as $field_name => $messages)
            {
                $ErrArr[$field_name] = $messages; 
            }
            return  response()->json($ErrArr);
        }

        $User =  User::find($request->user_id);

        // update  user
        $User->name = $request->name;
        $User->email = $request->email;
        $User->role = 'lab';
        $User->password =  Hash::make($request->password);
        $User->save();

        $Labs=Labs::where('user_id',$request->user_id)->first();

        // update laps request  DB
        $Labs->user_id = $User->id;
        $Labs->level = $request->level;
        $Labs->type = $request->type;
        $Labs->phone = $request->phone;
        $Labs->mobile = $request->mobile;
        $Labs->address = $request->address;
        $Labs->city = $request->city;
        $Labs->country = $request->country;
        $Labs->activities = $request->activities;
        $Labs->establish_year = $request->establish_year;
        $Labs->no_of_labs = $request->no_of_labs;
        $Labs->type_of_labs = $request->type_of_labs;
        $Labs->capacity_of_labs = $request->capacity_of_labs;
       //// upload  cv  ///////////////////////
       if ($request->hasFile('cv')) {
        if (file_exists(public_path("lap/cv/".$Labs->cv))) {
            @unlink(public_path("lap/cv/".$Labs->cv));
        }
        $cv=$request->cv;
        $name = $cv->getClientOriginalName(); // get image name
        $extension = $cv->getClientOriginalExtension(); // get image extension
        $sha1 = sha1($name); // hash the image name
        $random = rand(1, 1000000); // Random To Name
        $name_database = $random . "_" . date("y-m-d-h-i-s") . "_" . $sha1; // To use it without extension
        $fileName = $random . "_" . date("y-m-d-h-i-s") . "_" . $sha1 . "." . $extension;
        $cv->move(public_path('lap/cv'),  $fileName); //folder logo in public must you create
        $request->cv = $fileName;

    }

       ///////////////////// end  upload 
        $Labs->cv = $request->cv;
        $Labs->status = $request->status;
        $Labs->save();
        $data['data'] = ['msg' => 'Request Success','status' => 1];
        return response()->json($data);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Labs  $Labs
     * @return \Illuminate\Http\Response
     */
    public function destroy(Labs $Labs, Request $request)
    {

        $this->authorize('isAdmin');
        $this->rules['lap_id']='required';
       
        $validator = Validator::make($request->all(), $this->rules);
 
        if ($validator->fails()) {
            $statusCode = 400;
            $response["status"] = false;
            $response['message'] = $validator->errors()->all()[0];
            return response()->json($response, $statusCode);
 
        } 
      else{
 
         $Labs=Labs::find($request->lap_id);
         $Labs->delete();
         $statusCode = 200;
         $response["status"] = true;
         $response['message'] = "data delted successfully";
         return response()->json($response, $statusCode);
        }
    }

    public function search(Request $request)
    {

            //Validate Admin Auhtorization

            $this->authorize('isAdmin');

            //Initiate User Model

            $User=User::query();

        
            //Check for Name param

            if($request->filled('name'))
            {

                   $User->Where('name','LIKE','%'.$request->name.'%');


            }

            //Check for Email param

            if($request->filled('email'))
            {

                    $User->Where('email','LIKE','%'.$request->email.'%');


            }

            //Check for status param

            if($request->filled('status'))
            {

                $User->with(['lab' => function($User) use ($request){

                    $User->where('status', $request->status);

                }]);

            }

            //Check for Type param

            if($request->filled('type'))
            {

                $User->with(['lab' => function($User) use ($request){

                    $User->where('type', $request->type);

                }]);

            }

            //Check for Country param


            if($request->filled('country'))
            {

                $User->with(['lab' => function($User) use ($request){

                    $User->where('country', $request->country);

                }]);

            }

            //Check for City param

            if($request->filled('city'))
            {

                $User->with(['lab' => function($User) use ($request){

                    $User->where('city', $request->city);

                }]);

            }

             //Check for Mobile param

            if($request->filled('mobile'))
            {

                $User->with(['lab' => function($User) use ($request){

                    $User->where('mobile', $request->mobile);

                }]);

            }  
            
            //Check for Address param

            if($request->filled('address'))
            {

                $User->with(['lab' => function($User) use ($request){

                    $User->where('address', $request->address);

                }]);

            }     
            
             //Check for activities param
           
            if($request->filled('activities'))
            {

                $User->with(['lab' => function($User) use ($request){

                    $User->where('activities', $request->activities);

                }]);

            }     

             //Check for establish year param

            if($request->filled('establish_year'))
            {

                $User->with(['lab' => function($User) use ($request){

                    $User->where('establish_year', $request->establish_year);

                }]);

            }

            //Check for no of labs  param
            
            if($request->filled('no_of_labs'))
            {

                $User->with(['lab' => function($User) use ($request){

                    $User->where('no_of_labs', $request->no_of_labs);

                }]);

            }        
            
            //Check for no of type of labs  param
           
            if($request->filled('type_of_labs'))
            {

                $User->with(['lab' => function($User) use ($request){

                    $User->where('type_of_labs', $request->type_of_labs);

                }]);

            }        

             //Check for no of type of labs  param
                
            if($request->filled('capacity_of_labs'))
            {

                $User->with(['lab' => function($User) use ($request){

                    $User->where('capacity_of_labs', $request->capacity_of_labs);

                }]);

            }        
              //Check for type param

            
            if($request->filled('type'))
            {

                $User->with(['lab' => function($User) use ($request){

                    $User->where('type', $request->type);

                }]);

            }        

            //Check for capacity of labs param
           
            
            if($request->filled('capacity_of_labs'))
            {

                $User->with(['lab' => function($User) use ($request){

                    $User->where('capacity_of_labs', $request->capacity_of_labs);

                }]);

            }        


            //Check for Status of labs param
            
            
            if($request->filled('status'))
            {

                $User->with(['lab' => function($User) use ($request){

                    $User->where('status', $request->status);

                }]);

            }        
            
                          //Set Number of rows per page

            $num = 15;

            $User= $User->with('lab')->has('lab')->paginate($num);

            //Set Header Status Code

            $statusCode = 200;

            $response["status"] = 1;

            $response['data'] = $User;

            return response()->json($response, $statusCode);
            
    }


    public function guard()
    {
        return Auth::guard();
    }
}
