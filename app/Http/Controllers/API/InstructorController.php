<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\Models\Instructors;
use Mail;
use App\Mail\Active;
use App\Models\User;

use App\Http\Tools\RequestValidator as RV;
use  Validator;
use File;
use Illuminate\Support\Facades\Hash;
class InstructorController extends Controller
{
    //$RV => holds the validation controller

    protected $RV;

    //Constructing the auth controller and exclude create from authenticating

    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['store']]);

        //Assing the instance of Request Validator to protected variable $RV

        $this->RV = new RV;
    }

    public function index()
    {
        $this->authorize('isAdmin');
        $Instructors=User::with('instructor')->has('instructor')->paginate(15);

       if( $Instructors)
        {
        $statusCode = 200;
        $response["data"] =  $Instructors;
        $response["status"] = true;
        $response['message'] = "Request Successfully";
        return response()->json( $response,200);
        }
        else{
            $statusCode = 400;
            $response["status"] = false;
            $response['message'] = "there is no data to show ";
            return response()->json($response, $statusCode);
        }
    }

    public function store(Request $request)
    {
       //$ErrArr that holds error messages

        $ErrArr = [];
        
        //Start validating the request params
        
        $validator = $this->RV->Validate_Instructor((array) $request->all());

        //in case of validation failure output error messages and fields 

        if ($validator->fails())
        {
            foreach ($validator->messages()->getMessages() as $field_name => $messages)
            {
                $ErrArr[$field_name] = $messages[0]; 
            }
            return  response()->json($ErrArr);
        }

        // Creating new user

        $User = new User;
        $User->name = $request->name;
        $User->email = $request->email;
        $User->role = 'instructor';
        $User->password =  Hash::make($request->password);
        $User->save();

        // Creating instructor request to save it in the DB
        
        $Instructors = new Instructors;
        $Instructors->user_id = $User->id;
        $Instructors->level = $request->level;
        $Instructors->type = $request->type;
        $Instructors->phone = $request->phone;
        $Instructors->country = $request->country;
        $Instructors->address = $request->address;
        $Instructors->study_field = $request->study_field;
        $Instructors->graduation_year = $request->graduation_year;
        $Instructors->national_id = $request->national_id;

        
        if($request->hasFile('cv'))
        {
             $cv = $request->file('cv');
             $name = $cv->getClientOriginalName(); // get image name
             $extension = $cv->getClientOriginalExtension(); // get image extension
             $sha1 = sha1($name); // hash the image name
             $random = rand(1, 1000000); // Random To Name
             $name_database = $random . "_" . date("y-m-d-h-i-s") . "_" . $sha1; // To use it without extension
             $fileName = $random . "_" . date("y-m-d-h-i-s") . "_" . $sha1 . "." . $extension;
             $cv->move(public_path('instructor/cv'),  $fileName); //folder logo in public must you create
             $request->cv = $fileName;  
        }
      $Instructors->cv = $fileName;
      $Instructors->status = $request->status;
      $Instructors->save();
      $mail= Mail::to($request->email)->send(new Active($Instructors));
      $User->sendEmailVerificationNotification();

        $data['data'] = ['msg' => 'Request Success check your email to activate and verify account ','status' => 1];

        return response()->json($data);
    }

    public function update(Request $request)
    {
        $this->authorize('isAdmin');
        $ErrArr = [];
       $validator = $this->RV->Validate_updateinstructors((array) $request->all(),$request->user_id);
        if ($validator->fails())
        {
            foreach ($validator->messages()->getMessages() as $field_name => $messages)
            {
                $ErrArr[$field_name] = $messages; 
            }
            return  response()->json($ErrArr,400);
        }

       $User =  User::find($request->user_id);
       $Instructors=Instructors::where('user_id',$request->user_id)->first();

       $User->name = $request->name;
       $User->email = $request->email;
       $User->role = 'instructor';
       $User->password =  Hash::make($request->password);
       $User->save();

        $Instructors->user_id = $User->id;
        $Instructors->level = $request->level;
        $Instructors->type = $request->type;
        $Instructors->phone = $request->phone;
        $Instructors->country = $request->country;
        $Instructors->address = $request->address;
        $Instructors->study_field = $request->study_field;
        $Instructors->graduation_year = $request->graduation_year;
        $Instructors->national_id = $request->national_id;
        if ($request->hasFile('cv')) {
            if (file_exists(public_path("instructor/cv/".$Instructors->cv))) {
                @unlink(public_path("instructor/cv/".$Instructors->cv));
            }
            $cv=$request->cv;
            $name = $cv->getClientOriginalName(); // get image name
            $extension = $cv->getClientOriginalExtension(); // get image extension
            $sha1 = sha1($name); // hash the image name
            $random = rand(1, 1000000); // Random To Name
            $name_database = $random . "_" . date("y-m-d-h-i-s") . "_" . $sha1; // To use it without extension
            $fileName = $random . "_" . date("y-m-d-h-i-s") . "_" . $sha1 . "." . $extension;
            $cv->move(public_path('instructor/cv'),  $fileName); //folder logo in public must you create
            $request->cv = $fileName;

        }

        $Instructors->cv = $request->cv;
        $Instructors->status = 'pending';
        $Instructors->save();
        $data['data'] = ['msg' => 'Update Success','status' => 1];
        return response()->json($data, 200);


    }

    public function destory(Request $request)
    {

        $this->authorize('isAdmin');

        $this->rules['instructor_id']='required';
    
        $validator = Validator::make($request->all(), $this->rules);
 
        if ($validator->fails()) {
            $statusCode = 400;
            $response["status"] = false;
            $response['message'] = $validator->errors()->all()[0];
            return response()->json($response, $statusCode);
 
        } 
   
      else{
         
         $Instructors=Instructors::find($request->instructor_id);
         $Instructors->delete();
         $statusCode = 200;
         $response["status"] = true;
         $response['message'] = "data delted successfully";
         return response()->json($response, $statusCode);
 
        }
        
 
    }

    public function search(Request $request)
    {

        //Validate Admin Auhtorization

        $this->authorize('isAdmin');

        //Initiate Institutes Model

        $User=User::query();

        
        //Check for Name param

        if($request->filled('name'))
        {

               $User->Where('name','LIKE','%'.$request->name.'%');


        }

        //Check for Email param

        if($request->filled('email'))
        {

                $User->Where('email','LIKE','%'.$request->email.'%');


        }



            //Check for level param


        if($request->filled('level'))
            {

                $User->with(['instructor' => function($User) use ($request){

                    $User->where('level', $request->level);

                }]);

            }



            //Check for phone param


            if($request->filled('phone'))
            {

                $User->with(['instructor' => function($User) use ($request){

                    $User->where('phone', $request->phone);

                }]);

            }

            //Check for COuntry param

          
            if($request->filled('country'))
            {

                $User->with(['instructor' => function($User) use ($request){

                    $User->where('country', $request->country);

                }]);

            }

            //Check for City param

            if($request->filled('city'))
            {

                $User->with(['instructor' => function($User) use ($request){

                    $User->where('city', $request->city);

                }]);

            }
          

            //Check for Address param

            if($request->filled('address'))
            {

                $User->with(['instructor' => function($User) use ($request){

                    $User->where('address', $request->address);

                }]);

            }


            //Check for study field param


            if($request->filled('study_field'))
            {

                $User->with(['instructor' => function($User) use ($request){

                    $User->where('study_field', $request->study_field);

                }]);

            }



            //Check for Establish Year param

            if($request->filled('graduation_year'))
            {

                $User->with(['instructor' => function($User) use ($request){

                    $User->where('graduation_year', $request->graduation_year);

                }]);

            }

            //Check for national id param


            if($request->filled('national_id'))
            {

                $User->with(['instructor' => function($User) use ($request){

                    $User->where('national_id', $request->national_id);

                }]);

            }          

            //Check for Type param

            if($request->filled('type'))
            {

                $User->with(['instructor' => function($User) use ($request){

                    $User->where('type', $request->type);

                }]);

            }      
            
            //Check for Status param

            if($request->filled('status'))
            {

                $User->with(['instructor' => function($User) use ($request){

                    $User->where('status', $request->status);

                }]);

            }      
            


            //Set Number of rows per page

            $num = 15;
            $User= $User->with('instructor')->has('instructor')->paginate($num);


            //Set Header Status Code

            $statusCode = 200;

            $response["status"] = 1;

            $response['data'] =  $User;

            return response()->json($response, $statusCode);
            
   }
    

    public function guard()
    {
        return Auth::guard();
    }


   
}