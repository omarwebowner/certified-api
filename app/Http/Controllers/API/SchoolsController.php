<?php

namespace App\Http\Controllers\API;

use App\Models\Schools;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;
use App\Models\User;

use Mail;
use App\Mail\Active;

use Validator;
//use App\Models\Schools;
use App\Http\Tools\RequestValidator as RV;

class SchoolsController extends Controller
{
      //$RV => holds the validation controller

      protected $RV;

      //Constructing the auth controller and exclude create from authenticating
  
      public function __construct()
      {
          $this->middleware('auth:api', ['except' => ['store']]);
  
          //Assing the instance of Request Validator to protected variable $RV
  
          $this->RV = new RV;
      }
  
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->authorize('isAdmin');
         $schools=User::with('school')->has('school')->paginate(15);

        if($schools)
         { 
         $statusCode = 200;
         $response["data"] =    $schools;
         $response["status"] = true;
         $response['message'] = "Request Successfully";
         return response()->json( $response,200);
 
         }
         else{
             $statusCode = 400;
             $response["status"] = false;
             $response['message'] = "there is no data to show ";
             return response()->json($response, $statusCode);
         }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //$ErrArr that holds error messages

        $ErrArr = [];
                
        //Start validating the request params
        
        $validator = $this->RV->Validate_school((array) $request->all());

        //in case of validation failure output error messages and fields 

        if ($validator->fails())
        {
            foreach ($validator->messages()->getMessages() as $field_name => $messages)
            {
                $ErrArr[$field_name] = $messages[0]; 
            }
            return  response()->json($ErrArr);
        }

        //$file = $request->file('cv');

        // Creating new user
         //dd('done');
        $User = new User;
        $User->name = $request->name;
        $User->email = $request->email;
        $User->role = 'school';
        $User->password =  Hash::make($request->password);
        $User->save();

        // Creating instructor request to save it in the DB
        
        $Schools = new Schools;
        $Schools->user_id = $User->id;
        $Schools->level = $request->level;
        $Schools->type = $request->type;
        $Schools->phone = $request->phone;
        $Schools->mobile = $request->mobile;
        $Schools->address = $request->address;
        $Schools->city = $request->city;
        $Schools->country = $request->country;
        $Schools->activities = $request->activities;
        $Schools->establish_year = $request->establish_year;
        $Schools->no_of_labs = $request->no_of_labs;
        $Schools->type_of_labs = $request->type_of_labs;
        $Schools->capacity_of_labs = $request->capacity_of_labs;
         //// upload  cv  ///////////////////////
         if($request->hasFile('cv'))
         {
              $cv = $request->file('cv');
              $name = $cv->getClientOriginalName(); // get image name
              $extension = $cv->getClientOriginalExtension(); // get image extension
              $sha1 = sha1($name); // hash the image name
              $random = rand(1, 1000000); // Random To Name
              $name_database = $random . "_" . date("y-m-d-h-i-s") . "_" . $sha1; // To use it without extension
              $fileName = $random . "_" . date("y-m-d-h-i-s") . "_" . $sha1 . "." . $extension;
              $cv->move(public_path('school/cv'),  $fileName); //folder logo in public must you create
              $request->cv = $fileName;
  
              $Schools->cv = $fileName;
          }
         ///////////// end upload cv  //////////////
       

       ///////////////////// end  upload 
        $Schools->status = $request->status;
        $Schools->save();
        $mail= Mail::to($request->email)->send(new Active($Schools));

        $data['data'] = ['msg' => 'Request Success check your email to activate and verify account','status' => 1];
        return response()->json($data);
    }

  

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Schools  $schools
     * @return \Illuminate\Http\Response
     */
    public function show(Schools $schools)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Schools  $schools
     * @return \Illuminate\Http\Response
     */
    public function edit(Schools $schools)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Schools  $schools
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $this->authorize('isAdmin');

        $ErrArr = [];
                
        //Start validating the request params
        
        $validator = $this->RV->Validate_updatelschool((array) $request->all(),$request->user_id);

        //in case of validation failure output error messages and fields 

        if ($validator->fails())
        {
            foreach ($validator->messages()->getMessages() as $field_name => $messages)
            {
                $ErrArr[$field_name] = $messages; 
            }
            return  response()->json($ErrArr);
        }

        $User =  User::find($request->user_id);

        // update  user
        $User->name = $request->name;
        $User->email = $request->email;
        $User->role = 'school';
        $User->password =  Hash::make($request->password);
        $User->save();
        $Schools=Schools::where('user_id',$request->user_id)->first();

        // update laps request  DB
        $Schools->user_id = $User->id;
        $Schools->level = $request->level;
        $Schools->type = $request->type;
        $Schools->phone = $request->phone;
        $Schools->mobile = $request->mobile;
        $Schools->address = $request->address;
        $Schools->city = $request->city;
        $Schools->country = $request->country;
        $Schools->activities = $request->activities;
        $Schools->establish_year = $request->establish_year;
        $Schools->no_of_labs = $request->no_of_labs;
        $Schools->type_of_labs = $request->type_of_labs;
        $Schools->capacity_of_labs = $request->capacity_of_labs;
       //// upload  cv  ///////////////////////
       if ($request->hasFile('cv')) {
        if (file_exists(public_path("school/cv/".$Schools->cv))) {
            @unlink(public_path("school/cv/".$Schools->cv));
        }

        $cv=$request->cv;
        $name = $cv->getClientOriginalName(); // get image name
        $extension = $cv->getClientOriginalExtension(); // get image extension
        $sha1 = sha1($name); // hash the image name
        $random = rand(1, 1000000); // Random To Name
        $name_database = $random . "_" . date("y-m-d-h-i-s") . "_" . $sha1; // To use it without extension
        $fileName = $random . "_" . date("y-m-d-h-i-s") . "_" . $sha1 . "." . $extension;
        $cv->move(public_path('school/cv'),  $fileName); //folder logo in public must you create
        $request->cv = $fileName;
        $Schools->cv = $fileName;
    }

    
    $Schools->status = $request->status;
    $Schools->save();
    $data['data'] = ['msg' => 'Request Success','status' => 1];
    return response()->json($data);
}

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Schools  $schools
     * @return \Illuminate\Http\Response
     */
    public function destroy(Schools $schools,Request $request)
    {
        $this->authorize('isAdmin');
        $this->rules['school_id']='required';
      
         $validator = Validator::make($request->all(), $this->rules);
  
         if ($validator->fails()) {
             $statusCode = 400;
             $response["status"] = false;
             $response['message'] = $validator->errors()->all()[0];
             return response()->json($response, $statusCode);
  
         }   
       else{
  
          $schools=Schools::find($request->school_id);
          $schools->delete();
          $statusCode = 200;
          $response["status"] = true;
          $response['message'] = "data delted successfully";
          return response()->json($response, $statusCode);
  
             }
         
    }

    public function search(Request $request)
    {

            //Validate Admin Auhtorization

            $this->authorize('isAdmin');

            //Initiate User Model

            $User=User::query();

        
            //Check for Name param

            if($request->filled('name'))
            {

                   $User->Where('name','LIKE','%'.$request->name.'%');


            }

            //Check for Email param

            if($request->filled('email'))
            {

                    $User->Where('email','LIKE','%'.$request->email.'%');


            }

            //Check for status param

            if($request->filled('status'))
            {

                $User->with(['school' => function($User) use ($request){

                    $User->where('status', $request->status);

                }]);

            }

            //Check for Type param

            if($request->filled('type'))
            {

                $User->with(['school' => function($User) use ($request){

                    $User->where('type', $request->type);

                }]);

            }

            //Check for Country param


            if($request->filled('country'))
            {

                $User->with(['school' => function($User) use ($request){

                    $User->where('country', $request->country);

                }]);

            }

            //Check for City param

            if($request->filled('city'))
            {

                $User->with(['school' => function($User) use ($request){

                    $User->where('city', $request->city);

                }]);

            }

             //Check for Mobile param

            if($request->filled('mobile'))
            {

                $User->with(['school' => function($User) use ($request){

                    $User->where('mobile', $request->mobile);

                }]);

            }  
            
            //Check for Address param

            if($request->filled('address'))
            {

                $User->with(['school' => function($User) use ($request){

                    $User->where('address', $request->address);

                }]);

            }     
            
             //Check for activities param
           
            if($request->filled('activities'))
            {

                $User->with(['school' => function($User) use ($request){

                    $User->where('activities', $request->activities);

                }]);

            }     

             //Check for establish year param

            if($request->filled('establish_year'))
            {

                $User->with(['school' => function($User) use ($request){

                    $User->where('establish_year', $request->establish_year);

                }]);

            }

            //Check for no of labs  param
            
            if($request->filled('no_of_labs'))
            {

                $User->with(['school' => function($User) use ($request){

                    $User->where('no_of_labs', $request->no_of_labs);

                }]);

            }        
            
            //Check for no of type of labs  param
           
            if($request->filled('type_of_labs'))
            {

                $User->with(['school' => function($User) use ($request){

                    $User->where('type_of_labs', $request->type_of_labs);

                }]);

            }        

             //Check for no of type of labs  param
                
            if($request->filled('capacity_of_labs'))
            {

                $User->with(['school' => function($User) use ($request){

                    $User->where('capacity_of_labs', $request->capacity_of_labs);

                }]);

            }        
              //Check for type param

            
            if($request->filled('type'))
            {

                $User->with(['school' => function($User) use ($request){

                    $User->where('type', $request->type);

                }]);

            }        

            //Check for capacity of labs param
           
            
            if($request->filled('capacity_of_labs'))
            {

                $User->with(['school' => function($User) use ($request){

                    $User->where('capacity_of_labs', $request->capacity_of_labs);

                }]);

            }        


            //Check for Status of labs param
            
            
            if($request->filled('status'))
            {

                $User->with(['school' => function($User) use ($request){

                    $User->where('status', $request->status);

                }]);

            }        
            
                          //Set Number of rows per page

            $num = 15;

            $User= $User->with('school')->has('school')->paginate($num);

            //Set Header Status Code

            $statusCode = 200;

            $response["status"] = 1;

            $response['data'] = $User;

            return response()->json($response, $statusCode);
            
    }

    public function guard()
    {
        return Auth::guard();
    }
}
