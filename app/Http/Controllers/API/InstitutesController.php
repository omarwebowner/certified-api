<?php

namespace App\Http\Controllers\API;

use App\Models\Institutes;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;
use App\Models\User;
use Mail;
use App\Mail\Active;
use App\Http\Tools\RequestValidator as RV;
use Validator;

class InstitutesController extends Controller
{
      //$RV => holds the validation controller

      protected $RV;

      //Constructing the auth controller and exclude create from authenticating
  
      public function __construct()
      {
          $this->middleware('auth:api', ['except' => ['store']]);
          $this->RV = new RV;
      }
  
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->authorize('isAdmin');
        $institutes=User::with('institute')->has('institute')->paginate(15);

       if($institutes)
        {
        $statusCode = 200;
        $response["data"] = $institutes;
        $response["status"] = true;
        $response['message'] = "Request Successfully";
        return response()->json( $response,200);

        }
        else{
            $statusCode = 400;
            $response["status"] = false;
            $response['message'] = "There is no data to show ";
            return response()->json($response, $statusCode);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */


       /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $ErrArr = [];
                
        //Start validating the request params
        
        $validator = $this->RV->Validate_institute((array) $request->all());

        //in case of validation failure output error messages and fields 

        if ($validator->fails())
        {
            foreach ($validator->messages()->getMessages() as $field_name => $messages)
            {
                $ErrArr[$field_name] = $messages[0]; 
            }
            return  response()->json($ErrArr,400);
        }

        // Creating new user

        $User = new User;
        $User->name = $request->name;
        $User->email = $request->email;
        $User->role = 'institute';
        $User->password =  Hash::make($request->password);
        $User->save();

        // Creating instructor request to save it in the DB
        
        $Institutes = new Institutes;
        $Institutes->user_id = $User->id;
        $Institutes->level = $request->level;
        $Institutes->type = $request->type;
        $Institutes->phone = $request->phone;
        $Institutes->mobile = $request->mobile;
        $Institutes->address = $request->address;
        $Institutes->city = $request->city;
        $Institutes->country = $request->country;
        $Institutes->activities = $request->activities;
        $Institutes->establish_year = $request->establish_year;
        $Institutes->no_of_labs = $request->no_of_labs;
        $Institutes->type_of_labs = $request->type_of_labs;
        $Institutes->capacity_of_labs = $request->capacity_of_labs;
        $fileName = '';

           if($request->hasFile('cv'))
           {
                $cv = $request->file('cv');
                $name = $cv->getClientOriginalName(); // get image name
                $extension = $cv->getClientOriginalExtension(); // get image extension
                $sha1 = sha1($name); // hash the image name
                $random = rand(1, 1000000); // Random To Name
                $name_database = $random . "_" . date("y-m-d-h-i-s") . "_" . $sha1; // To use it without extension
                $fileName = $random . "_" . date("y-m-d-h-i-s") . "_" . $sha1 . "." . $extension;
                $cv->move(public_path('institute/cv'),  $fileName); //folder logo in public must you create
           }
      //  $Institutes->cv = $request->cv;
       $Institutes->cv = $fileName;
        $Institutes->status = $request->status;
        $Institutes->save();

        $mail= Mail::to($request->email)->send(new Active( $Institutes));
        $data['data'] = ['msg' => 'Request Success','status' => 1];
        return response()->json($data,200);
    }

  
   

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Institutes  $Institutes
     * @return \Illuminate\Http\Response
     */
    public function show(Institutes $Institutes)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Institutes  $Institutes
     * @return \Illuminate\Http\Response
     */
    public function edit(Institutes $Institutes)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Institutes  $Institutes
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Institutes $Institutes)
    {       
        $this->authorize('isAdmin');
        $ErrArr = [];
        $User =  User::find($request->user_id);
        $Institutes=Institutes::where('user_id',$request->user_id)->first();

        $validator = $this->RV->Validate_updateinstitute((array) $request->all(),$request->user_id);
        $User->name = $request->name;
        $User->email = $request->email;
        $User->role = 'institute';
        $User->password =  Hash::make($request->password);
        $User->save();

        if ($validator->fails())
        {
            foreach ($validator->messages()->getMessages() as $field_name => $messages)
            {
                $ErrArr[$field_name] = $messages; 
            }
            return  response()->json($ErrArr,400);
        }
        $Institutes->user_id = $User->id;
        $Institutes->level = $request->level;
        $Institutes->type = $request->type;
        $Institutes->phone = $request->phone;
        $Institutes->mobile = $request->mobile;
        $Institutes->address = $request->address;
        $Institutes->city = $request->city;
        $Institutes->country = $request->country;
        $Institutes->activities = $request->activities;
        $Institutes->establish_year = $request->establish_year;
        $Institutes->no_of_labs = $request->no_of_labs;
        $Institutes->type_of_labs = $request->type_of_labs;
        $Institutes->capacity_of_labs = $request->capacity_of_labs;
        if ($request->hasFile('cv')) {
            if (file_exists(public_path("institute/cv/".$Institutes->cv))) {
                @unlink(public_path("institute/cv/".$Institutes->cv));
            }
            $cv=$request->cv;
            $name = $cv->getClientOriginalName(); // get image name
            $extension = $cv->getClientOriginalExtension(); // get image extension
            $sha1 = sha1($name); // hash the image name
            $random = rand(1, 1000000); // Random To Name
            $name_database = $random . "_" . date("y-m-d-h-i-s") . "_" . $sha1; // To use it without extension
            $fileName = $random . "_" . date("y-m-d-h-i-s") . "_" . $sha1 . "." . $extension;
            $cv->move(public_path('institute/cv'),  $fileName); //folder logo in public must you create
            $request->cv = $fileName;
        }
        $Institutes->cv = $request->cv;
        $Institutes->status = 'pending';
        $Institutes->save();

        $data['data'] = ['msg' => 'Update Success','status' => 1];
        return response()->json($data, 200);
    }
    

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Institutes  $Institutes
     * @return \Illuminate\Http\Response
     */
    public function destroy(Institutes $Institutes ,Request $request)
    {
    $this->authorize('isAdmin');
     $this->rules['institute_id']='required';
       $validator = Validator::make($request->all(), $this->rules);

       if ($validator->fails()) {
           $statusCode = 400;
           $response["status"] = false;
           $response['message'] = $validator->errors()->all()[0];
           return response()->json($response, $statusCode);
       } 
      
     else{
        $Institutes=Institutes::find($request->institute_id);
        $Institutes->delete();
        $statusCode = 200;
        $response["status"] = true;
        $response['message'] = "data delted successfully";
        return response()->json($response, $statusCode);
        }
    }

    public function search(Request $request)
    {

            //Validate Admin Auhtorization

            $this->authorize('isAdmin');

            //Initiate User Model

            $User=User::query();

        
            //Check for Name param

            if($request->filled('name'))
            {

                   $User->Where('name','LIKE','%'.$request->name.'%');


            }

            //Check for Email param

            if($request->filled('email'))
            {

                    $User->Where('email','LIKE','%'.$request->email.'%');


            }

            //Check for status param

            if($request->filled('status'))
            {

                $User->with(['institute' => function($User) use ($request){

                    $User->where('status', $request->status);

                }]);

            }

            //Check for Type param

            if($request->filled('type'))
            {

                $User->with(['institute' => function($User) use ($request){

                    $User->where('type', $request->type);

                }]);

            }

            //Check for Country param


            if($request->filled('country'))
            {

                $User->with(['institute' => function($User) use ($request){

                    $User->where('country', $request->country);

                }]);

            }

            //Check for City param

            if($request->filled('city'))
            {

                $User->with(['institute' => function($User) use ($request){

                    $User->where('city', $request->city);

                }]);

            }

             //Check for Mobile param

            if($request->filled('mobile'))
            {

                $User->with(['institute' => function($User) use ($request){

                    $User->where('mobile', $request->mobile);

                }]);

            }  
            
            //Check for Address param

            if($request->filled('address'))
            {

                $User->with(['institute' => function($User) use ($request){

                    $User->where('address', $request->address);

                }]);

            }     
            
             //Check for activities param
           
            if($request->filled('activities'))
            {

                $User->with(['institute' => function($User) use ($request){

                    $User->where('activities', $request->activities);

                }]);

            }     

             //Check for establish year param

            if($request->filled('establish_year'))
            {

                $User->with(['institute' => function($User) use ($request){

                    $User->where('establish_year', $request->establish_year);

                }]);

            }

            //Check for no of labs  param
            
            if($request->filled('no_of_labs'))
            {

                $User->with(['institute' => function($User) use ($request){

                    $User->where('no_of_labs', $request->no_of_labs);

                }]);

            }        
            
            //Check for no of type of labs  param
           
            if($request->filled('type_of_labs'))
            {

                $User->with(['institute' => function($User) use ($request){

                    $User->where('type_of_labs', $request->type_of_labs);

                }]);

            }        

             //Check for no of type of labs  param
                
            if($request->filled('capacity_of_labs'))
            {

                $User->with(['institute' => function($User) use ($request){

                    $User->where('capacity_of_labs', $request->capacity_of_labs);

                }]);

            }        
              //Check for type param

            
            if($request->filled('type'))
            {

                $User->with(['institute' => function($User) use ($request){

                    $User->where('type', $request->type);

                }]);

            }        

            //Check for capacity of labs param
           
            
            if($request->filled('capacity_of_labs'))
            {

                $User->with(['institute' => function($User) use ($request){

                    $User->where('capacity_of_labs', $request->capacity_of_labs);

                }]);

            }        


            //Check for Status of labs param
            
            
            if($request->filled('status'))
            {

                $User->with(['institute' => function($User) use ($request){

                    $User->where('status', $request->status);

                }]);

            }        
            
                          //Set Number of rows per page

            $num = 15;

            $User= $User->with('institute')->has('institute')->paginate($num);

            //Set Header Status Code

            $statusCode = 200;

            $response["status"] = 1;

            $response['data'] = $User;

            return response()->json($response, $statusCode);
            
    }

    public function guard()
    {
        return Auth::guard();
    }

    
}
