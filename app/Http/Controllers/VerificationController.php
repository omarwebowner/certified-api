<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;

class VerificationController extends Controller
{
    
    public function verify($user_id, Request $request) {
        // if (!$request->hasValidSignature()) {
        //     return response()->json(["msg" => "Invalid/Expired url provided."], 401);
        // }
    
        $user = User::findOrFail($user_id);
        //dd()
    
        if (!$user->hasVerifiedEmail()) {
            $user->markEmailAsVerified();
        }
    
        return redirect()->to('https://motamd.org/login');
    }
    
    public function resend($id) {
       // dd('test');
       $user=User::find($id);
      // dd($user);
        if ( $user->hasVerifiedEmail()) {
            return response()->json(["msg" => "Email already verified."], 400);
        }
    
        $user->sendEmailVerificationNotification();
        return redirect()->to('https://motamd.org/');
        
    }

}
