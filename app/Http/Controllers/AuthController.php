<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Validator;
use Mail;
use Hash;
use App\Mail\Resset;
use App\Models\User;
use URL;

class AuthController extends Controller
{
    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['login','reset_password']]);
    }

    /**
     * Get a JWT token via given credentials.
     *
     * @param  \Illuminate\Http\Request  $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request)
    {
        $credentials = $request->only('email', 'password');

        if ($token = $this->guard()->attempt($credentials)) {
            return $this->respondWithToken($token);
        }

        return response()->json(['error' => 'Unauthorized'], 401);
    }

    /**
     * Get the authenticated User
     *
     * @return \Illuminate\Http\JsonResponse
     */
   
    public function me()
    {
        if( $this->guard()->user()->role != 'admin'){
            $this->guard()->user()->requestCertified;
            $this->guard()->user()->requestCertified->cv = asset('/'.$this->guard()->user()->role.'/cv/'.$this->guard()->user()->requestCertified->cv);
        }
    
        return response()->json($this->guard()->user());
    }
    /**
     * Log the user out (Invalidate the token)
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {
        $this->guard()->logout();

        return response()->json(['message' => 'Successfully logged out']);
    }

    public function reset_password(Request $request)
    { 
       //dd('asd');
  
        $validator = Validator::make($request->all(), [
          
            'email' => 'required|email',
  
        ]);
  
        if ($validator->fails()) {
            $statusCode = 400;
            $response["status"] = false;
            $response['message'] = $validator->errors()->all();
            return response()->json($response, $statusCode);     
  
        } else {
  
         //dd($request->email);
          $user=User::where('email',$request->email)->first();
         // dd($user);
          if ($user) {
           
            // dd($user);
            $random = rand('111111','999999');
            $user->password=$random;
            // dd( $random); // 193872
            $user->password=bcrypt( $random);
              $user->save();
           
             // dd( $user);
             $mail= Mail::to($request->email)->send(new Resset($random));
          
             $statusCode = 200;
             $response['status'] = true;
             $response['message'] =  "Success reset";
             //$response['data'] = $user;
            
             return response()->json($response, $statusCode);   
             if($mail)
             { 
                
               $statusCode = 200;
               $response['status'] = true;
               $response['message'] = "Success reset";
               //$response['data'] = $user;
              
               return response()->json($response, $statusCode);   
             }
  
          } 
          else{
              // if  credentials not true
              $statusCode = 400;
              $response['status'] = false;
              $response['message'] = "Email not found ";
              return response()->json($response, $statusCode);
  
          }
        }
  
    
  
  }
  

    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh()
    {
        return $this->respondWithToken($this->guard()->refresh());
    }

    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken($token)
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'role' => $this->guard()->user()->role,
            'expires_in' => $this->guard()->factory()->getTTL() * 60
        ]);
    }

    /**
     * Get the guard to be used during authentication.
     *
     * @return \Illuminate\Contracts\Auth\Guard
     */
    public function guard()
    {
        return Auth::guard();
    }
}