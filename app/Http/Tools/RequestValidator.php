<?php

namespace App\Http\Tools;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class RequestValidator extends Controller
{

    public function Validate_Instructor(array $data){
        return Validator::make($data, [
            'name' => 'required|Regex:/^[\D]+$/i|max:100',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6',
            'level'=>'required|max:150',
            'type'=>'required',
            'address'=>'required',
            'phone'=>'required',
            'country'=>'required',
            'national_id'=>'required',
            'study_field'=>'required',
            'graduation_year'=>'required',
            'cv'=>'required|mimes:png,jpg,pdf,jpeg',
             ]);
          }

          public function Validate_updateinstructors(array $data,$user)
          {

            return Validator::make($data, [
              'name' => 'required|Regex:/^[\D]+$/i|max:100',
              'user_id'=>'required|numeric',
              'email' => 'required|unique:users,email,'.$user,
             // 'password' => 'required|min:6',
              'level'=>'required|max:150',
              'type'=>'required',
              'address'=>'required',
              'phone'=>'required',
              'country'=>'required',
              'study_field'=>'required',
              'graduation_year'=>'required',
              'cv'=>'somtimes|nullable|mimes:png,jpg,pdf,jpeg',    
               ]);

          }

          public function Validate_institute(array $data){
            return Validator::make($data, [
                'name' => 'required|Regex:/^[\D]+$/i|max:100',
                'email' => 'required|email|max:255|unique:users',
                'password' => 'required|min:6',
                'level'=>'required|max:150',
                'type'=>'required',
                'address'=>'required',
                'mobile'=>'required',
                'phone'=>'required',
                'country'=>'required',
                'city'=>'required',
                'activities'=>'required',
                'establish_year'=>'required',
                'no_of_labs'=>'required',
                'type_of_labs'=>'required',
                'capacity_of_labs'=>'required',
                'cv'=>'required|mimes:jpeg,pdf,jpg,png|max:10000',
                 ]);
              }

              public function Validate_institutesearch(array $data){
                return Validator::make($data, [
                   'user_id'=>'sometimes|nullable|numeric',
                    'name' => 'sometimes|nullable|Regex:/^[\D]+$/i|max:100',
                    'email' => 'sometimes|nullable|email|max:255',
                    'level'=>'sometimes|nullable|max:150',
                    'type'=>'sometimes|nullable',
                    'address'=>'sometimes|nullable',
                    'mobile'=>'sometimes|nullable',
                    'phone'=>'sometimes|nullable',
                    'country'=>'sometimes|nullable',
                    'city'=>'sometimes|nullable',
                    'activities'=>'sometimes|nullable',
                    'establish_year'=>'sometimes|nullable',
                    'no_of_labs'=>'sometimes|nullable',
                    'type_of_labs'=>'sometimes|nullable',
                    'capacity_of_labs'=>'sometimes|nullable',
                    'cv'=>'sometimes|nullable|mimes:jpeg,pdf,jpg,png|max:10000',
                     ]);
                  }

              public function Validate_updateinstitute(array $data,$user){
               
                return Validator::make($data, [
                    'name' => 'required|Regex:/^[\D]+$/i|max:100',
                    'user_id'=>'required|numeric',
                    'email' => 'required|unique:users,email,'.$user,
                   // 'password' => 'required|min:6',
                    'level'=>'required|max:150',
                    'type'=>'required',
                    'address'=>'required',
                    'mobile'=>'required',
                    'phone'=>'required',
                    'country'=>'required',
                    'city'=>'required',
                    'activities'=>'required',
                    'establish_year'=>'required',
                    'no_of_labs'=>'required',
                    'type_of_labs'=>'required',
                    'capacity_of_labs'=>'required',
                     ]);
                  }


                  public function Validate_lap(array $data){
                    return Validator::make($data, [
                        'name' => 'required|Regex:/^[\D]+$/i|max:100',
                        'email' => 'required|email|max:255|unique:users',
                        'password' => 'required|min:6',
                        'level'=>'required|max:150',
                        'type'=>'required',
                        'address'=>'required',
                        'mobile'=>'required',
                        'phone'=>'required',
                        'country'=>'required',
                        'city'=>'required',
                        'activities'=>'required',
                        'establish_year'=>'required',
                        'no_of_labs'=>'required',
                        'type_of_labs'=>'required',
                        'capacity_of_labs'=>'required',
                        'cv'=>'required|mimes:png,jpg,pdf,jpeg',        
                         ]);
                      }

                      public function Validate_updatelap(array $data,$user)
                      {

                        return Validator::make($data, [
                          'name' => 'required|Regex:/^[\D]+$/i|max:100',
                          'email' => 'required|email|max:255|unique:users,email,'.$user,
                          'user_id' => 'required|numeric',
                         // 'password' => 'required|min:6',
                          'level'=>'required|max:150',
                          'type'=>'required',
                          'address'=>'required',
                          'mobile'=>'required',
                          'phone'=>'required',
                          'country'=>'required',
                          'city'=>'required',
                          'activities'=>'required',
                          'establish_year'=>'required',
                          'no_of_labs'=>'required',
                          'type_of_labs'=>'required',
                          'capacity_of_labs'=>'required',
                          //'cv'=>'required|mimes:docx,csv,xlsx,xls,doc,ppt,pptx,odt,ods,odp,pdf,zip| max:10000',
                          'cv'=>'sometimes|nullable|mimes:jpeg,pdf,jpg,png|max:10000',

                          'status'=>'required',
          
                           ]);

                      }

                      public function Validate_school(array $data){
                        return Validator::make($data, [
                            'name' => 'required|Regex:/^[\D]+$/i|max:100',
                            'email' => 'required|email|max:255|unique:users',
                            'password' => 'required|min:6',
                            'level'=>'required|max:150',
                            'type'=>'required',
                            'address'=>'required',
                            'mobile'=>'required',
                            'phone'=>'required',
                            'country'=>'required',
                            'city'=>'required',
                            'activities'=>'required',
                            'establish_year'=>'required',
                            'no_of_labs'=>'required',
                            'type_of_labs'=>'required',
                            'capacity_of_labs'=>'required',
                            'cv'=>'required|mimes:png,jpg,pdf,jpeg',
            
                             ]);
                          }
                          public function Validate_updatelschool(array $data,$user)
                          {

                            return Validator::make($data, [
                              'name' => 'required|Regex:/^[\D]+$/i|max:100',
                              'email' => 'required|email|max:255|unique:users,email,'.$user,
                              'user_id'=>'required|numeric',
                             
                             // 'password' => 'required|min:6',
                              'level'=>'required|max:150',
                              'type'=>'required',
                              'address'=>'required',
                              'mobile'=>'required',
                              'phone'=>'required',
                              'country'=>'required',
                              'city'=>'required',
                              'activities'=>'required',
                              'establish_year'=>'required',
                              'no_of_labs'=>'required',
                              'type_of_labs'=>'required',
                              'capacity_of_labs'=>'required',
                              //'cv'=>'required|mimes:docx,csv,xlsx,xls,doc,ppt,pptx,odt,ods,odp,pdf,zip| max:10000',
                              'cv'=>'sometimes|nullable|mimes:jpeg,pdf,jpg,png|max:10000',

                              'status'=>'required',
              
                               ]);


                          }
    
    
                        }



