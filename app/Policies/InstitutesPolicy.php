<?php

namespace App\Policies;

use App\Models\User;
use App\Models\Institutes;

use Illuminate\Auth\Access\HandlesAuthorization;

class InstitutesPolicy extends SitePolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }


    public function viewAny(User $user, Institutes $institutes)
    {
        $this->authorize('viewAny', $institutes);
    }
    public function view(User $user, Institutes $institutes)
    {
        $this->authorize('view', $institutes);
    }

    public function update(User $user, Post $post)
    {
        return $user->id === $post->user_id;
    }
    /**
     * Determine whether the user can delete the post.
     *
     * @param  \App\User  $user
     * @param  \App\post  $post
     * @return mixed
     */
    public function delete(User $user, Post $post)
    {
        return $user->id === $post->user_id;
    }
}
