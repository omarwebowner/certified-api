<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Auth::routes(['verify' => true]);

////////////////////////////////////
Route::group([
    'namespace' => 'App\Http\Controllers',
    'prefix' => 'auth'

], function ($router) {
   
    Route::get('email/verify/{id}', 'VerificationController@verify')->name('verification.verify'); // Make sure to keep this as your route name
    Route::get('email/resend/{id}', 'VerificationController@resend')->name('verification.resend');
    
});


/////////////////////////////////////////

Route::group([
    'middleware' => 'api',
    'namespace' => 'App\Http\Controllers',
    'prefix' => 'auth'

], function ($router) {

    Route::post('login', 'AuthController@login')->name('login');
    Route::post('logout', 'AuthController@logout');
    Route::post('refresh', 'AuthController@refresh');
    Route::post('me', 'AuthController@me');
    Route::post('reset_password', 'AuthController@reset_password')->name('reset_password');
    Route::get('email/verify/{id}', 'VerificationController@verify')->name('verification.verify'); // Make sure to keep this as your route name
    Route::get('email/resend/{id}', 'VerificationController@resend')->name('verification.resend');
    

});


Route::group([

  //  'middleware' => 'api',
    'namespace' => 'App\Http\Controllers\API',
    'prefix' => 'request'

], function ($router) {

    Route::post('instructor', 'InstructorController@store');
    Route::post('school', 'SchoolsController@store');
    Route::post('institute', 'InstitutesController@store');
    Route::post('lab', 'LabsController@store');


});

/////////// admin  ///////////////
Route::group([
    'namespace' => 'App\Http\Controllers\API',
    'prefix' => 'admin'
], function ($router) {
    Route::get('institutes', 'InstitutesController@index');
    Route::post('institutes/store', 'InstitutesController@store');
    Route::post('institutes/update', 'InstitutesController@update');
    Route::post('institutes/delete', 'InstitutesController@destroy');
    Route::get('institutes/search', 'InstitutesController@search');

    Route::get('instructor', 'InstructorController@index');
    Route::post('instructor/store', 'InstructorController@store');
    Route::post('instructor/update', 'InstructorController@update');
    Route::post('instructor/delete', 'InstructorController@destory');
    Route::get('instructor/search', 'InstructorController@search');

    Route::get('lab', 'LabsController@index');
    Route::post('lab/store', 'LabsController@store');
    Route::post('lab/update', 'LabsController@update');
    Route::post('lab/delete', 'LabsController@destroy');
    Route::get('lab/search', 'LabsController@search');

    Route::get('school', 'SchoolsController@index');
    Route::post('school/store', 'SchoolsController@store');
    Route::post('school/update', 'SchoolsController@update');
    Route::post('school/delete', 'SchoolsController@destroy');
    Route::get('school/search', 'SchoolsController@search');

});

Route::fallback(function() {
    return response()->json([
        'data' => [],
        'success' => false,
        'status' => 404,
        'message' => 'Invalid Route'
    ]);
});
