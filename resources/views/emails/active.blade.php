@component('mail::message')
@php
    $user=App\Models\User::find($data->user_id);
@endphp
Hi {{  $user->name }}!
<br />
Thanks for applying for a certified  
<br />
Please confirm your email address to complete your application.
<br />
Confirm your email
<br />
@component('mail::button', ['url' => url('api/auth/email/resend/'.$data->user_id)])
Activate Your Account
@endcomponent
<br />
<h3>Or Active Your Account By Click</h3>
<br />
{{ url('api/auth/email/resend/'.$data->user_id) }}
<br /><br />
Thanks,<br>
{{ config('app.name') }}
@endcomponent
