<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatelabsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('labs', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users');
            $table->string('level');
            $table->string('type');
            $table->string('phone');
            $table->string('mobile');
            $table->text('address');
            $table->text('city');
            $table->string('country');
            $table->text('activities');
            $table->string('establish_year');
            $table->string('no_of_labs');
            $table->string('type_of_labs');
            $table->string('capacity_of_labs');
            $table->string('cv');
            $table->string('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('labs');
    }
}
